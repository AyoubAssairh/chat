import firebase from "firebase";
import firestore from "firebase/firestore";

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyARfZMsCNe_-5vlB2BeaHThK5moOBtiVjo",
  authDomain: "udemy-ninja-chat-134e8.firebaseapp.com",
  databaseURL: "https://udemy-ninja-chat-134e8.firebaseio.com",
  projectId: "udemy-ninja-chat-134e8",
  storageBucket: "udemy-ninja-chat-134e8.appspot.com",
  messagingSenderId: "206389032587",
  appId: "1:206389032587:web:d4c3c4aef131236149468e",
  measurementId: "G-0FG8KQXZMH"
};
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);
firebase.analytics();

export default firebaseApp.firestore();
